﻿import * as express from 'express';
import * as passport from 'passport';
import * as expressSession from 'express-session';
import * as LocalStrategy from 'passport-local';
import * as bodyParser from 'body-parser';
import * as cookieParser from 'cookie-parser';
import * as mongoose from 'mongoose';
import * as bcrypt from 'bcrypt';

import { User } from './models/user.model';


const port  = 1337;
const dbUrl = 'mongodb://db_access:verystrongpassword@ds157980.mlab.com:57980/kotprog';

var app = express();
app.set('dbUrl', dbUrl);
mongoose.connect(dbUrl);

app.use(bodyParser.urlencoded({ 'extended': 'true' }));
app.use(cookieParser());

passport.serializeUser((user, done) => {
    done(null, user);
});

passport.deserializeUser((user, done) => {
    done(null, user);
});

passport.use('login', new LocalStrategy.Strategy((username, password, done) => {
    User.findOne({ username: username }, function (err, temp_user) {
        if (err) { return done(err); }
        if (!temp_user) { return done(null, false); }
        temp_user.comparePassword(password, function (err, isMatch) {
            if (err) throw err;
            if (!isMatch)
                done("Invalid password", temp_user);
            else
                done(null, temp_user);
        });
    });
}));

app.use(expressSession({ secret: 'verysecretsomething12345' }));
app.use(passport.initialize());
app.use(passport.session());

app.use('/', require('./routes/user.route')(passport, express.Router()));

app.listen(port, () => {
    console.log("Server is running.");
});