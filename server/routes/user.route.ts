﻿import { User, Patient, Doctor } from '../models/user.model';

module.exports = (passport, router) => {

    // Regisztráció. (POST)
    // Input: 
    //    - username (string): a felhasználónév amivel regisztrálni akarunk
    //    - password (string): a jelszó a regisztrációhoz
    //    - doctor (1 vagy 0): orvosként (1) vagy páciensként (0) regisztrálunk?
    //
    // Return: Szöveges üzenet a sikerességről.
    router.post('/register', (req, res, next) => {
        var username = req.body.username;
        var password = req.body.password;
        var doctor = req.body.doctor;
        if (!username || !password || !doctor) {
            res.status(500).send("Missing registration info.");
        } else {
            User.find({ 'username': username }, (error, result) => {
                if (error) {
                    res.status(500).send("Error");
                }
                else if (result.length != 0) {
                    res.status(500).send("User already registered with that username!");
                }
                else {
                    var user;
                    if (doctor == true) {
                        user = new Doctor({ username: username, password: password });
                    }
                    else {
                        user = new Patient({ username: username, password: password });
                    }
                    user.save();
                    res.status(200).send("Registration successful!");
                }
            });
        }
    })

    // Belépés. (POST)
    // Input:
    //    - username (string): a felhasználónév amivel regisztráltunk
    //    - password (string): a regisztrációkor megadott jelszó
    //
    // Return: Szöveges üzenet a sikerességről.
    router.post('/login', (req, res, next) => {
        if (req.isAuthenticated()) {
            return res.status(200).send("Already logged in!");
        }

        passport.authenticate('login', (error, user) => {
            if (error) {
                console.log(error);
                res.status(500).send("Invalid password.");
            }
            else {
                req.logIn(user, (error) => {
                    if (error) {
                        console.log(error);
                        res.status(500).send("Invalid username.");
                    }
                    else {
                        console.log(user);
                        res.status(200).send("Login SUCCESSFUL!");
                    }
                })
            }
        })(req, res, next);
    })

    // Kijelentkezés. (POST)
    // Input: none
    // Return: Szöveges üzenet a sikerességről.
    router.post('/logout', (req, res, next) => {
        if (!req.isAuthenticated()) {
            return res.status(200).send("You are not logged in.");
        }

        req.logOut();
        res.status(200).send("Successfully logged out.");
    })

    // Saját profil lekérése. (GET)
    // Input: none
    // Return: A profil JSON formában siker esetén, szöveges üzenet hiba esetén.
    router.get('/self', (req, res, next) => {
        if (req.isAuthenticated()) {
            res.status(200).send(req.user);
        }
        else {
            res.status(500).send("Not logged in.");
        }
    })

    // Az összes páciens kilistázása a rendszerben, orvosoktól függetlenül. (GET)
    // Input: none
    // Return: A páciensek profiljainak listája JSON-ban, vagy szöveges üzenet hiba esetén.
    router.get('/listpatients', (req, res, next) => {
        if (!req.isAuthenticated())
            return res.status(500).send("Not logged in.");

        if (req.user.kind != 'Doctor')
            return res.status(500).send("You do not have the permission to view this page.");

        User.find({ 'kind': 'Patient' }, (error, result) => {
            if (error) {
                res.status(500).send("Error");
            }
            else {
                // Remove the password from the results
                for (let patient of result)
                    delete patient._doc.password;

                res.status(200).send(result);
            }
        });
    })

    // Páciens hozzáadása a jelenleg belogolt orvoshoz. (POST)
    // Input:
    //    - id: a hozzáadni kívánt páciens ID-ja
    //
    // Return: Szöveges üzenet a sikerességről.
    router.post('/addpatient', (req, res, next) => {
        if (!req.isAuthenticated())
            return res.status(500).send("Not logged in.");

        if (req.user.kind != 'Doctor')
            return res.status(500).send("You do not have the permission to access this page.");

        if (!req.body.id)
            return res.status(500).send("Missing user ID.");

        User.findById(req.body.id, (error, result) => {
            if (error) {
                res.status(500).send("Error: Cannot find user.");
            }
            else {
                req.user.patients.push(result._id + "");

                // Update database
                Doctor.update({ _id: req.user }, req.user, (err, n, r) => {
                    if (err)
                        res.status(500).send("Error: Could not add patient.");
                    else {
                        req.logIn(req.user, (error) => {
                            if (error)
                                res.status(500).send("Error: Could not login user.");
                            else
                                res.status(200).send("Patient added successfully!");
                        });
                    }
                });
            }
        });
    })

    // A jelenleg belogolt orvoshoz tartozó páciensek listázása. (GET)
    // Input: none
    // Return: A páciensek listája JSON-ban, vagy szöveges üzenet hiba esetén.
    router.get('/mypatients', (req, res, next) => {
        if (!req.isAuthenticated())
            return res.status(500).send("Not logged in.");

        if (req.user.kind != 'Doctor')
            return res.status(500).send("You do not have the permission to access this page.");

        User.find({ '_id': { $in: req.user.patients } }, (err, docs) => {
            if (err)
                return res.status(500).send(err);

            // Remove the password from the results
            for (let patient of docs)
                delete patient._doc.password;

            res.status(200).send(docs);
        });
    })

    // Mérés hozzáadása egy pácienshez. (POST)
    // Csak olyan pácienshez adható mérés, amely az éppen belogolt orvoshoz tartozik.
    // Input:
    //    - id: a páciens ID-ja
    //    - date: a mérés ideje (formátum pl.: "2017-05-08")
    //    - type: a mérés típusa (pl.: "Height", "Weight")
    //    - value: a mérés értéke
    //
    // Return: Szöveges üzenet a sikerességről.
    router.post('/addmeasurement', (req, res, next) => {
        if (!req.isAuthenticated())
            return res.status(500).send("Not logged in.");

        if (req.user.kind != 'Doctor')
            return res.status(500).send("You do not have the permission to access this page.");

        if (!req.body.id) return res.status(500).send("Missing patient ID.");
        if (!req.body.date) return res.status(500).send("Missing field: 'date'.");
        if (!req.body.type) return res.status(500).send("Missing field: 'type'.");
        if (!req.body.value) return res.status(500).send("Missing field: 'value'.");

        if (req.user.patients.indexOf(req.body.id) == -1)
            return res.status(500).send("Attempting to modify an unassigned patient.");

        Patient.findById(req.body.id, (error, result) => {
            if (error) {
                res.status(500).send("Error: Cannot find patient.");
            }
            else {
                result.measurements.push({
                    date: req.body.date,
                    type: req.body.type,
                    value: req.body.value
                });

                // Update database
                Patient.update({ _id: req.body.id }, result, (err, n, r) => {
                    if (err)
                        res.status(500).send("Error: Could not update patient.");
                    else
                        res.status(200).send("Measurement added successfully!");
                });
            }
        });
    })

    // Saját profil állítása. (POST)
    // Csak a következő adatok módosíthatók:
    //    - password: a felhasználó jelszava
    //    - phoneNumber: a felhasználó telefonszáma
    //
    // Input: Kulcs-érték párok halmaza, ahol a kulcs a fentiek valamelyike, az érték pedig az új érték amire
    //        cserélni szeretnénk. Egy kérésben több adat is módosítható, ha több kulcsot adunk meg.
    //
    // Return: Szöveges üzenet a sikerességről.
    router.post('/setprofile', (req, res, next) => {
        if (!req.isAuthenticated())
            return res.status(500).send("Not logged in.");

        User.findById(req.user._id, (error, result) => {
            if (error) {
                res.status(500).send("Error: Cannot find user.");
            }
            else {
                let changes = {};
                if (req.body.password) changes['password'] = req.body.password;
                if (req.body.phoneNumber) changes['phoneNumber'] = req.body.phoneNumber;

                // Merge into session
                req.user = { ...req.user, ...changes };

                // Update database
                User.update({ _id: req.user }, changes, (err, n, r) => {
                    if (err)
                        res.status(500).send("Error: Could not update user.");
                    else {
                        req.logIn(req.user, (error) => {
                            if (error)
                                res.status(500).send("Error: Could not login user.");
                            else
                                res.status(200).send("Data changed successfully!");
                        });
                    }
                });
            }
        });
    })

    return router;
}